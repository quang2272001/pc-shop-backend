import { DataSource } from 'typeorm';
import { config } from 'dotenv';
config();

export const AppDataSource = new DataSource({
  type: 'mysql',
  username: process.env.DATABASE_USER_NAME,
  password: process.env.DATABASE_PASSWORD,
  port: Number(process.env.DATABASE_PORT) || 3306,
  database: process.env.DATABASE_NAME,
  host: process.env.DATABASE_HOST || 'localhost',
  entities: ['libs/database/src/entities/*{.ts,.js}'],
  migrations: ['libs/database/src/migrations/*.ts'],
});
