import { InjectQueue } from '@nestjs/bull';
import { Injectable, Logger } from '@nestjs/common';
import { CreateOrderDto } from 'apps/api/src/module/order/dto/create-order.dto';
import { Queue } from 'bull';

@Injectable()
export class QueueService {
  constructor(@InjectQueue('order') private readonly orderQueue: Queue) {}
  logger = new Logger(QueueService.name);
  async createOrder(userId: number, createOrderData: CreateOrderDto) {
    try {
      this.logger.debug('start order');
      const job = await this.orderQueue.add('createOrder', {
        userId,
        createOrderData,
      });
      const data = await job.finished();
      this.logger.debug('order success');
      return data;
    } catch (error) {
      this.logger.debug(error);
    }
  }
}
