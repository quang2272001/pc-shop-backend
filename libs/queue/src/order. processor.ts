import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { OrderService } from 'apps/api/src/module/order/order.service';
import { Job } from 'bull';

@Processor('order')
export class OrderProcessor {
  constructor(private readonly orderService: OrderService) {}
  private logger = new Logger(OrderProcessor.name);
  @Process('createOrder')
  async createOrder(job: Job<any>) {
    try {
      const { userId, createOrderData } = job.data;
      return await this.orderService.createOrder(userId, createOrderData);
    } catch (error) {
      throw error;
    }
  }
}
