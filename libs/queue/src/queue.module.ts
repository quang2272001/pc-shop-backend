import { Module, forwardRef } from '@nestjs/common';
import { BullModule } from '@nestjs/bull';
import { OrderModule } from 'apps/api/src/module/order/order.module';
import { QueueService } from './queue.service';
import { OrderProcessor } from './order. processor';

@Module({
  imports: [
    BullModule.forRoot({
      redis: {
        host: 'redis-17670.c267.us-east-1-4.ec2.cloud.redislabs.com',
        port: 17670,
        password: 'Cammich1308',
      },
    }),
    BullModule.registerQueue({ name: 'order' }),
    forwardRef(() => OrderModule),
  ],
  providers: [QueueService, OrderProcessor],
  exports: [QueueService],
})
export class QueueModule {}
