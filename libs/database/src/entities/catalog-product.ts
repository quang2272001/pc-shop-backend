import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Product } from './product.entity';
import { Catalog } from './catalog.entity';

@Entity()
export class CatalogProduct {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'product_id' })
  productId: number;

  @Column({ name: 'catalog_id' })
  catalogId: number;

  @ManyToOne(() => Product, (product) => product.listCatalog)
  @JoinColumn({ name: 'product_id' })
  product: Product;

  @ManyToOne(() => Catalog, (catalog) => catalog.listProduct)
  @JoinColumn({ name: 'catalog_id' })
  catalog: Catalog;

  constructor($id: number) {
    this.id = $id;
  }

  /**
   * Getter $id
   * @return {number}
   */
  public get $id(): number {
    return this.id;
  }

  /**
   * Setter $id
   * @param {number} value
   */
  public set $id(value: number) {
    this.id = value;
  }
}
