import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { OrderProduct } from './order-product.entity';
import { CatalogProduct } from './catalog-product';
import { ProductBill } from './product-bill.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column('mediumtext')
  description: string;

  @Column()
  quantity: number;

  @Column()
  properties: string;

  @Column({ type: 'simple-array', nullable: true })
  images: string[];

  @Column({ name: 'is_selected', default: false })
  isSelected: boolean;

  @Column({ name: 'is_delete', default: false })
  isDelete: boolean;

  @OneToMany(() => OrderProduct, (orderProduct) => orderProduct.product)
  listOrder: OrderProduct[];

  @OneToMany(() => CatalogProduct, (catalogProduct) => catalogProduct.product)
  listCatalog: CatalogProduct[];

  @OneToMany(() => ProductBill, (productBill) => productBill.product)
  listBill: ProductBill[];
}
