import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { ProductBill } from './product-bill.entity';
import { Supplier } from './supplier.entity';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdDate: Date;

  @Column({ name: 'supplier_id' })
  supplierId: number;

  @OneToMany(() => ProductBill, (productBill) => productBill.bill)
  listProduct: ProductBill[];

  @ManyToOne(() => Supplier, (supplier) => supplier.listBill)
  @JoinColumn({ name: 'supplier_id' })
  supplier: Supplier;

  constructor($id: number, $createdDate: Date) {
    this.id = $id;
    this.createdDate = $createdDate;
  }

  /**
   * Getter $id
   * @return {number}
   */
  public get $id(): number {
    return this.id;
  }

  /**
   * Getter $createdDate
   * @return {Date}
   */
  public get $createdDate(): Date {
    return this.createdDate;
  }

  /**
   * Setter $id
   * @param {number} value
   */
  public set $id(value: number) {
    this.id = value;
  }

  /**
   * Setter $createdDate
   * @param {Date} value
   */
  public set $createdDate(value: Date) {
    this.createdDate = value;
  }
}
