import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CatalogProduct } from './catalog-product';

@Entity()
export class Catalog {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  image: string;

  @OneToMany(() => CatalogProduct, (catalogProduct) => catalogProduct.catalog)
  listProduct: CatalogProduct[];
}
