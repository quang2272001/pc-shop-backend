import {
  Column,
  Entity,
  Index,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity()
export class User {
  constructor(
    $email: string,
    $password: string,
    $fullname: string,
    $username: string,
    $address: string,
    $dob: Date,
    $phoneNumber: string,
    $role: number,
  ) {
    this.email = $email;
    this.password = $password;
    this.fullname = $fullname;
    this.username = $username;
    this.address = $address;
    this.dob = $dob;
    this.phoneNumber = $phoneNumber;
    this.role = $role;
  }
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  fullname: string;

  @Column({ unique: true })
  username: string;

  @Column()
  address: string;

  @Column()
  dob: Date;

  @Column()
  phoneNumber: string;

  @Column()
  role: number;

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  public get $role(): number {
    return this.role;
  }

  public set $role(value: number) {
    this.role = value;
  }

  public getId(): number {
    return this.id;
  }

  public setId(id: number): void {
    this.id = id;
  }

  public getEmail(): string {
    return this.email;
  }

  public setEmail(email: string): void {
    this.email = email;
  }

  public getPassword(): string {
    return this.password;
  }

  public setPassword(password: string): void {
    this.password = password;
  }

  public getFullname(): string {
    return this.fullname;
  }

  public setFullname(fullname: string): void {
    this.fullname = fullname;
  }

  public getUsername(): string {
    return this.username;
  }

  public setUsername(username: string): void {
    this.username = username;
  }

  public getAddress(): string {
    return this.address;
  }

  public setAddress(address: string): void {
    this.address = address;
  }

  public getDob(): Date {
    return this.dob;
  }

  public setDob(dob: Date): void {
    this.dob = dob;
  }

  public getPhoneNumber(): string {
    return this.phoneNumber;
  }

  public setPhoneNumber(phoneNumber: string): void {
    this.phoneNumber = phoneNumber;
  }
}
