import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Product } from './product.entity';
import { Bill } from './bill.entity';

@Entity()
export class ProductBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'product_id' })
  productId: number;

  @Column({ name: 'bill_id' })
  billId: number;

  @ManyToOne(() => Product, (product) => product.listBill)
  product: Product;

  @ManyToOne(() => Bill, (bill) => bill.listProduct)
  bill: Bill;

  constructor($id: number) {
    this.id = $id;
  }

  /**
   * Getter $id
   * @return {number}
   */
  public get $id(): number {
    return this.id;
  }

  /**
   * Setter $id
   * @param {number} value
   */
  public set $id(value: number) {
    this.id = value;
  }
}
