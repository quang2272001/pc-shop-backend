import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Store } from './entities/store.entity';
import { Bill } from './entities/bill.entity';
import { Catalog } from './entities/catalog.entity';
import { CatalogProduct } from './entities/catalog-product';
import { Order } from './entities/order.entity';
import { OrderProduct } from './entities/order-product.entity';
import { Product } from './entities/product.entity';
import { ProductBill } from './entities/product-bill.entity';
import { Supplier } from './entities/supplier.entity';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        const database = config.get('database');
        return Object.assign(database.mysql, {
          entities: [
            User,
            Store,
            Bill,
            Catalog,
            CatalogProduct,
            Order,
            OrderProduct,
            Product,
            ProductBill,
            Supplier,
          ],
        });
      },
    }),
  ],
})
export class DatabaseModule {}
