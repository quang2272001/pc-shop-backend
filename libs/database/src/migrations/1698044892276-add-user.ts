import { MigrationInterface, QueryRunner } from "typeorm";

export class AddUser1698044892276 implements MigrationInterface {
    name = 'AddUser1698044892276'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` int NOT NULL AUTO_INCREMENT, \`email\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`fullname\` varchar(255) NOT NULL, \`username\` varchar(255) NOT NULL, \`address\` varchar(255) NOT NULL, \`dob\` datetime NOT NULL, \`phoneNumber\` varchar(255) NOT NULL, \`role\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`user\``);
    }

}
