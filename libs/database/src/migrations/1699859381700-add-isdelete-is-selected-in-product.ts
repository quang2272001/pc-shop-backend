import { MigrationInterface, QueryRunner } from "typeorm";

export class AddIsdeleteIsSelectedInProduct1699859381700 implements MigrationInterface {
    name = 'AddIsdeleteIsSelectedInProduct1699859381700'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`product\` ADD \`is_selected\` tinyint NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE \`product\` ADD \`is_delete\` tinyint NOT NULL DEFAULT 0`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`product\` DROP COLUMN \`is_delete\``);
        await queryRunner.query(`ALTER TABLE \`product\` DROP COLUMN \`is_selected\``);
    }

}
