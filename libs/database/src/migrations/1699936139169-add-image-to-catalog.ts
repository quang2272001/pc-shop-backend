import { MigrationInterface, QueryRunner } from "typeorm";

export class AddImageToCatalog1699936139169 implements MigrationInterface {
    name = 'AddImageToCatalog1699936139169'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`catalog\` ADD \`image\` varchar(255) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`catalog\` DROP COLUMN \`image\``);
    }

}
