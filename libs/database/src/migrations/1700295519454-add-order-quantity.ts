import { MigrationInterface, QueryRunner } from "typeorm";

export class AddOrderQuantity1700295519454 implements MigrationInterface {
    name = 'AddOrderQuantity1700295519454'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_product\` ADD \`quantity\` int NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`order_product\` DROP COLUMN \`quantity\``);
    }

}
