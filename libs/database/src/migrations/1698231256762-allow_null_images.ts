import { MigrationInterface, QueryRunner } from "typeorm";

export class AllowNullImages1698231256762 implements MigrationInterface {
    name = 'AllowNullImages1698231256762'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`product\` CHANGE \`images\` \`images\` text NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`product\` CHANGE \`images\` \`images\` text NOT NULL`);
    }

}
