import { Injectable } from '@nestjs/common';
import { UploadApiErrorResponse, UploadApiResponse, v2 } from 'cloudinary';
import toStream = require('buffer-to-stream');
@Injectable()
export class CloudinaryService {
  async uploadImage(
    file: Express.Multer.File,
  ): Promise<UploadApiResponse | UploadApiErrorResponse> {
    return new Promise((resolve, reject) => {
      const upload = v2.uploader.upload_stream((error, result) => {
        if (error) return reject(error);
        resolve(result);
      });

      toStream(file.buffer).pipe(upload);
    });
  }

  async deleteImage(deleteList: Array<string>) {
    v2.api.delete_resources(deleteList, function (error, result) {
      if (error) throw error;
      return result;
    });
  }

  async uploadListImage(files: Express.Multer.File[]) {
    const listPromise = [];
    for (const file of files) {
      listPromise.push(this.uploadImage(file));
    }

    const result = await Promise.all(listPromise);
    return result.map((file) => file.url);
  }
}
