import { Module } from '@nestjs/common';
import { CachingService } from './caching.service';
import { redisStore } from 'cache-manager-redis-store';
import { CacheModule } from '@nestjs/cache-manager';
import { RedisClientOptions } from 'redis';

@Module({
  imports: [
    CacheModule.register<RedisClientOptions>({
      store: (): any =>
        redisStore({
          socket: {
            host: 'redis-17670.c267.us-east-1-4.ec2.cloud.redislabs.com',
            port: 17670,
          },
          password: 'Cammich1308',
        }),
    }),
  ],
  providers: [CachingService],
  exports: [CachingService],
})
export class CachingModule {}
