import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './module/auth/auth.module';
import { UserModule } from './module/user/user.module';
import configuration from './configuration/configuration';
import { DatabaseModule } from '@/database';
import { ProductModule } from './module/product/product.module';
import { CatalogModule } from './module/catalog/catalog.module';
import { OrderModule } from './module/order/order.module';
import { OrderProductModule } from './module/order-product/order-product.module';
import { QueueModule } from '@app/queue';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [configuration] }),
    DatabaseModule,
    AuthModule,
    UserModule,
    ProductModule,
    CatalogModule,
    OrderModule,
    OrderProductModule,
    QueueModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
