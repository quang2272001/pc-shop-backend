import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('health check')
@Controller()
export class AppController {
  @Get()
  getHello() {
    return {
      message: 'well come to my website',
    };
  }
}
