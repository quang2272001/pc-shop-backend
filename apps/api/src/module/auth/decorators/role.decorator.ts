import { SetMetadata } from '@nestjs/common';
import { ROLE } from 'apps/api/src/common/constants';

export const ROLES_KEY = 'role';
export const Roles = (...role: ROLE[]) => SetMetadata(ROLES_KEY, role);
