import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { LoginDto, RegisterDto } from './dto/auth.dto';
import { AuthService } from './auth.service';

@ApiTags('User')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  async register(@Body() data: RegisterDto) {
    await this.authService.register({ ...data, dob: data.dob || new Date() });
    return {
      success: true,
    };
  }

  @Post('login')
  async login(@Body() data: LoginDto) {
    return await this.authService.login(data.email, data.password);
  }
}
