import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { RegisterDto } from './dto/auth.dto';
import { ERROR_MESSAGE, ROLE } from '../../common/constants';
import { comparePassword, hashPassword } from '../../common/functions';
import { JwtService } from '@nestjs/jwt';
import configuration from '../../configuration/configuration';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async register(data: RegisterDto) {
    return await this.userService.createUser({
      ...data,
      password: await hashPassword(data.password),
      role: ROLE.USER,
    });
  }

  async login(email: string, userPassword: string) {
    const user = await this.userService.findOneUser({ email: email });
    if (!user?.password || !comparePassword(userPassword, user?.password)) {
      throw new UnauthorizedException(
        ERROR_MESSAGE.username_or_password_incorect,
      );
    }
    const payload = { sub: user.getId(), email: user.getUsername() };
    const { password, ...userInfo } = user;
    return {
      data: {
        access_token: await this.jwtService.signAsync(payload, {
          secret: configuration().auth.jwtsecret,
          expiresIn: configuration().auth.access_token_ttl,
        }),
        user: userInfo,
      },
      success: true,
    };
  }
}
