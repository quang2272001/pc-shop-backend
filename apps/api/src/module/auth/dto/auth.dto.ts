import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsDateString,
  IsNumberString,
  IsOptional,
} from 'class-validator';

export class RegisterDto {
  @ApiProperty({ required: true })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  password: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  fullname: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  username: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  address: string;

  @ApiProperty({ required: true })
  @IsOptional()
  @IsDateString()
  dob: Date;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumberString()
  phoneNumber: string;
}

export class LoginDto {
  @ApiProperty({ required: true, example: 'admin1@gmail.com' })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ required: true, example: '123123' })
  @IsNotEmpty()
  password: string;
}
