import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { User } from '../auth/decorators/user.decorator';
import { JWTAuthGuard } from '../auth/jwt-auth.guard';
import { ApiBearerAuth, ApiTags, ApiQuery } from '@nestjs/swagger';
import { RolesGuard } from '../auth/role/role.guard';
import { ApiPaginage } from '../../common/decorator/api-paginage.decorator';
import { SortOptionPipe } from '../../common/validation.pipe';
import { FindOptionsOrder } from '../../common/dto/find.dto';
import { QueueService } from '@app/queue';
import { ORDER_STATUS, ROLE } from '../../common/constants';
import { Roles } from '../auth/decorators/role.decorator';

@Controller('order')
@UseGuards(JWTAuthGuard, RolesGuard)
@ApiBearerAuth()
@ApiTags('Order')
export class OrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly queueService: QueueService,
  ) {}

  @Post()
  async create(@User() user, @Body() createOrderDto: CreateOrderDto) {
    return await this.queueService.createOrder(user.id, createOrderDto);
  }

  @Get()
  @ApiPaginage()
  @ApiQuery({ name: 'status', required: false })
  async findAll(
    @User() user,
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('keyword') keyword: string,
    @Query('status') status: ORDER_STATUS,
    @Query('sort', SortOptionPipe) sort: FindOptionsOrder,
  ) {
    return await this.orderService.findAllOrder(user, {
      page: page || 1,
      limit: limit || 10,
      keyword: keyword || '',
      sort: sort || { id: 'DESC' },
      status: status || ORDER_STATUS.all,
    });
  }

  @Get('admin/getall')
  @ApiPaginage()
  @ApiQuery({ name: 'status', required: false })
  @Roles(ROLE.ADMIN)
  async findAllAdmin(
    @User() user,
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('keyword') keyword: string,
    @Query('status') status: ORDER_STATUS,
    @Query('sort', SortOptionPipe) sort: FindOptionsOrder,
  ) {
    return await this.orderService.findAllOrderAdmin({
      page: page || 1,
      limit: limit || 10,
      keyword: keyword || '',
      sort: sort || { id: 'DESC' },
      status: status || ORDER_STATUS.all,
    });
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.orderService.findOneOrder(+id);
  }

  @Roles(ROLE.ADMIN)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateOrderDto: UpdateOrderDto,
  ) {
    return await this.orderService.updateOrder({ id: +id }, updateOrderDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return await this.orderService.removeOrder(+id);
  }
}
