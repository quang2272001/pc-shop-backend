import { ORDER_STATUS } from 'apps/api/src/common/constants';
import { FindAllParam } from 'apps/api/src/common/dto/find.dto';

export interface FindAllOrderParams extends FindAllParam {
  status: ORDER_STATUS;
}
