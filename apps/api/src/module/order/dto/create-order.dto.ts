import { ApiProperty } from '@nestjs/swagger';
import { RECEIVE_TYPE } from 'apps/api/src/common/constants';
import {
  IsArray,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  Min,
  ValidateNested,
} from 'class-validator';

export class ProductInfo {
  @ApiProperty({ required: true })
  @IsNumber()
  id: number;

  @ApiProperty({ required: true })
  @IsNumber()
  @Min(1)
  quantity: number;
}

export class CreateOrderDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  nameReceiver: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  addressReceiver: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  phoneReceiver: string;

  @ApiProperty({ required: true })
  @IsEnum(RECEIVE_TYPE)
  type: number;

  @ApiProperty({ isArray: true, type: () => ProductInfo })
  @IsArray()
  @ValidateNested({ each: true })
  productInfos: ProductInfo[];
}
