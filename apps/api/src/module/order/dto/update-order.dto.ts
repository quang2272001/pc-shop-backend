import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateOrderDto } from './create-order.dto';
import { IsEnum } from 'class-validator';
import { ORDER_STATUS } from 'apps/api/src/common/constants';

export class UpdateOrderDto extends PartialType(CreateOrderDto) {
  @ApiProperty({ required: true })
  @IsEnum(ORDER_STATUS)
  status: string;
}
