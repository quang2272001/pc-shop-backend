import { Order } from '@/database/entities/order.entity';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class OrderRepository extends Repository<Order> {
  constructor(datasource: DataSource) {
    super(Order, datasource.createEntityManager());
  }
}
