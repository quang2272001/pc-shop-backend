import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { OrderRepository } from './order.repository';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { OrderProductModule } from '../order-product/order-product.module';
import { QueueModule } from '@app/queue';
import { ProductModule } from '../product/product.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    OrderProductModule,
    QueueModule,
    ProductModule,
  ],
  controllers: [OrderController],
  providers: [OrderService, OrderRepository],
  exports: [OrderService],
})
export class OrderModule {}
