import { OrderProductService } from './../order-product/order-product.service';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { OrderRepository } from './order.repository';
import { FindAllParam } from '../../common/dto/find.dto';
import { DeepPartial, FindOptionsWhere, Like } from 'typeorm';
import { Order } from '@/database/entities/order.entity';
import { ORDER_STATUS, ROLE } from '../../common/constants';
import { ProductService } from '../product/product.service';
import { FindAllOrderParams } from './dto/find-order.dto';

@Injectable()
export class OrderService {
  constructor(
    private readonly orderRepository: OrderRepository,
    private readonly orderProductService: OrderProductService,
    private readonly productService: ProductService,
  ) {}

  async createOrder(userId, createOrderDto: CreateOrderDto) {
    try {
      const { productInfos, ...createOrderData } = createOrderDto;
      console.log(productInfos);
      let products = await this.productService.findProductIn(
        productInfos.map(({ id }) => id),
      );
      products = products.map((v, i) => {
        v.quantity = v.quantity - productInfos[i].quantity;
        if (v.quantity < 0) throw new BadRequestException();
        return v;
      });
      if (productInfos.length !== products.length) {
        return new BadRequestException('product not found');
      }
      const order = await this.orderRepository.save({
        ...createOrderData,
        userId: userId,
        status: ORDER_STATUS.created,
      });
      await this.orderProductService.createBulkOrderProduct({
        orderId: order.id,
        productIds: productInfos,
      });
      await this.productService.updateQuantityProduct(products);
      return { success: true };
    } catch (error) {
      return error;
    }
  }

  async findAllOrder(user, params: FindAllOrderParams) {
    const { page, limit, keyword, sort, status } = params;
    const take = (page - 1) * limit;
    const where = status ? { status } : {};
    const [data, total] = await this.orderRepository.findAndCount({
      where: {
        user: { username: Like(`%${keyword}%`), id: user.id },
        ...where,
      },
      relations: { listProduct: { product: true } },
      order: sort,
      take,
    });
    const totalPage = Math.ceil(total / limit);
    return {
      data: data.map((order) => {
        return {
          ...order,
          listProduct: order.listProduct.map(({ product, quantity }) => {
            return {
              product,
              quantity,
            };
          }),
        };
      }),
      paginage: {
        page,
        totalPage,
      },
    };
  }

  async findAllOrderAdmin(params: FindAllOrderParams) {
    const { page, limit, keyword, sort, status } = params;
    const take = (page - 1) * limit;
    const where = status ? { status } : {};
    const [data, total] = await this.orderRepository.findAndCount({
      where: {
        user: { username: Like(`%${keyword}%`) },
        ...where,
      },
      relations: { listProduct: { product: true } },
      order: sort,
      take,
    });
    const totalPage = Math.ceil(total / limit);
    return {
      data: data.map((order) => {
        return {
          ...order,
          listProduct: order.listProduct.map(({ product, quantity }) => {
            return {
              product,
              quantity,
            };
          }),
        };
      }),
      paginage: {
        page,
        totalPage,
      },
    };
  }

  async findOneOrder(id: number) {
    const order = await this.orderRepository.findOne({
      where: { id },
      relations: { listProduct: { product: true } },
      loadRelationIds: {
        relations: ['user'],
      },
    });
    if (!order) throw new BadRequestException();
    return {
      success: true,
      data: {
        ...order,
        listProduct: order.listProduct.map(({ quantity, product }) => {
          return { quantity, product };
        }),
      },
    };
  }

  async updateOrder(where: FindOptionsWhere<Order>, data: DeepPartial<Order>) {
    await this.orderRepository.update(where, data);
    return {
      success: true,
    };
  }

  async removeOrder(id: number) {
    await this.orderRepository.delete({ id });
    return {
      success: true,
    };
  }
}
