import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { CatalogService } from './catalog.service';
import { CreateCatalogDto } from './dto/create-catalog.dto';
import { UpdateCatalogDto } from './dto/update-catalog.dto';
import { ApiBearerAuth, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { JWTAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role/role.guard';
import { SortOptionPipe } from '../../common/validation.pipe';
import { FindOptionsOrder } from '../../common/dto/find.dto';
import { ApiPaginage } from '../../common/decorator/api-paginage.decorator';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('catalog')
@ApiTags('Catalog')
@ApiBearerAuth()
@UseGuards(JWTAuthGuard, RolesGuard)
export class CatalogController {
  constructor(private readonly catalogService: CatalogService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  createCatalog(
    @Body() createCatalogDto: CreateCatalogDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.catalogService.createCatalog(createCatalogDto, file);
  }

  @Get()
  @ApiPaginage()
  async findAllCatalog(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('keyword') keyword: string,
    @Query('sort', SortOptionPipe) sort: FindOptionsOrder,
  ) {
    return await this.catalogService.findAllCatalog({
      page: page || 1,
      limit: limit || 10,
      keyword: keyword || '',
      sort: sort || { id: 'DESC' },
    });
  }

  @Get(':id')
  async findOneCatalog(@Param('id') id: string) {
    return await this.catalogService.findOneCatalog({ id: +id });
  }

  @Patch(':id')
  async updateCatalog(
    @Param('id') id: string,
    @Body() updateCatalogDto: UpdateCatalogDto,
  ) {
    return await this.catalogService.updateCatalog(
      { id: +id },
      updateCatalogDto,
    );
  }

  @Delete(':id')
  async removeCatalog(@Param('id') id: string) {
    return await this.catalogService.removeCatalog(+id);
  }
}
