import { Module } from '@nestjs/common';
import { CatalogService } from './catalog.service';
import { CatalogController } from './catalog.controller';
import { CatalogRepository } from './catalog.repository';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Catalog } from '@/database/entities/catalog.entity';
import { CatalogProduct } from '@/database/entities/catalog-product';
import { CloudinaryModule } from '@app/cloudinary';

@Module({
  imports: [
    UserModule,
    AuthModule,
    TypeOrmModule.forFeature([Catalog, CatalogProduct]),
    CloudinaryModule,
  ],
  controllers: [CatalogController],
  providers: [CatalogService, CatalogRepository],
  exports: [CatalogService],
})
export class CatalogModule {}
