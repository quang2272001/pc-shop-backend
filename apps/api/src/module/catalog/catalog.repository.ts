import { Catalog } from '@/database/entities/catalog.entity';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class CatalogRepository extends Repository<Catalog> {
  constructor(datasource: DataSource) {
    super(Catalog, datasource.createEntityManager());
  }
}
