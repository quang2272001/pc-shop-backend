import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCatalogDto } from './dto/create-catalog.dto';
import { UpdateCatalogDto } from './dto/update-catalog.dto';
import { CatalogRepository } from './catalog.repository';
import { FindAllParam } from '../../common/dto/find.dto';
import { FindOptionsWhere, In, Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CatalogProduct } from '@/database/entities/catalog-product';
import { Catalog } from '@/database/entities/catalog.entity';
import { CloudinaryService } from '@app/cloudinary';

@Injectable()
export class CatalogService {
  constructor(
    private catalogRepository: CatalogRepository,
    @InjectRepository(CatalogProduct)
    private catalogProductRepo: Repository<CatalogProduct>,
    private imageService: CloudinaryService,
  ) {}
  async createCatalog(
    createCatalogDto: CreateCatalogDto,
    file: Express.Multer.File,
  ) {
    const images = await this.imageService.uploadListImage([file]);
    await this.catalogRepository.save({
      ...createCatalogDto,
      image: images[0],
    });
    return { success: true };
  }

  async findAllCatalog(params: FindAllParam) {
    const { page, limit, keyword, sort } = params;
    const take = (page - 1) * limit;
    const [data, total] = await this.catalogRepository.findAndCount({
      where: { name: Like(`%${keyword}%`) },
      take,
      order: sort,
    });
    const totalPage = Math.ceil(total / limit);

    return {
      success: true,
      data,
      paginage: {
        page,
        totalPage,
      },
    };
  }

  async findOneCatalog(where: FindOptionsWhere<Catalog>) {
    const catalog = await this.catalogRepository.findOneBy(where);
    if (!catalog) throw new BadRequestException();
    return {
      success: true,
      data: catalog,
    };
  }

  async updateCatalog(
    where: FindOptionsWhere<Catalog>,
    updateCatalogDto: UpdateCatalogDto,
  ) {
    await this.findOneCatalog(where);
    await this.catalogRepository.update(where, updateCatalogDto);
    return {
      success: true,
    };
  }

  async removeCatalog(id: number) {
    await this.findOneCatalog({ id });
    await this.catalogRepository.delete({ id });
    return {
      success: true,
    };
  }

  async addProductToCatalog(productId: number, listCatalogId: number[]) {
    const listProductCatalog = [];
    for (const catalogId of listCatalogId) {
      listProductCatalog.push({ catalogId, productId });
    }
    await this.catalogProductRepo.save(listProductCatalog);
  }

  async deleteProductCatalog(catalogIds: number[]) {
    await this.catalogProductRepo.delete({ catalogId: In(catalogIds) });
  }
}
