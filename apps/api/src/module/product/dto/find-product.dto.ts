import { FindAllParam } from 'apps/api/src/common/dto/find.dto';

export interface FindAllProductParam extends FindAllParam {
  catalogId: number;
}
