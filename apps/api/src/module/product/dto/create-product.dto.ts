import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  MaxLength,
} from 'class-validator';

export class CreateProductDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @MaxLength(200)
  name: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  price: number;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @MaxLength(2000)
  description: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  quantity: number;

  @ApiProperty({ required: true })
  @MaxLength(500)
  @IsNotEmpty()
  properties: string;

  @ApiProperty({
    required: false,
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
  })
  @IsOptional()
  @IsArray()
  images: string[];

  @ApiProperty({
    required: true,
    type: 'array',
    items: {
      type: 'number',
      format: 'number',
    },
  })
  @Transform(({ value }) =>
    Array.isArray(value) ? value : value.split(',').map((num) => Number(num)),
  )
  @IsNumber({}, { each: true })
  catalogIds: number[];
}
