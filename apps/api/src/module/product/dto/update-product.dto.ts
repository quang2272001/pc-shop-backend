import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateProductDto } from './create-product.dto';
import { ProductInfo } from '../../order/dto/create-order.dto';
import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  MaxLength,
} from 'class-validator';

export class UpdateProductDto {
  @ApiProperty({ required: true })
  @IsOptional()
  @MaxLength(200)
  name?: string;

  @ApiProperty({ required: true })
  @IsOptional()
  price?: number;

  @ApiProperty({ required: true })
  @IsOptional()
  @MaxLength(2000)
  description?: string;

  @ApiProperty({ required: true })
  @IsOptional()
  quantity?: number;

  @ApiProperty({ required: true })
  @MaxLength(500)
  @IsOptional()
  properties?: string;

  @ApiProperty({
    required: true,
    type: 'array',
    items: {
      type: 'number',
      format: 'number',
    },
  })
  @IsOptional()
  @IsNumber({}, { each: true })
  catalogIds?: number[];

  isDelete?: boolean;
}

export class CartDto {
  @ApiProperty({ required: true, isArray: true, type: ProductInfo })
  @IsNotEmpty()
  @IsArray()
  cart: ProductInfo[];
}
