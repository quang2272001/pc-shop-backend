import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseGuards,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './dto/create-product.dto';
import { CartDto, UpdateProductDto } from './dto/update-product.dto';
import { ApiBearerAuth, ApiConsumes, ApiQuery, ApiTags } from '@nestjs/swagger';
import { SortOptionPipe } from '../../common/validation.pipe';
import { FindOptionsOrder } from '../../common/dto/find.dto';
import { JWTAuthGuard } from '../auth/jwt-auth.guard';
import { RolesGuard } from '../auth/role/role.guard';
import { Roles } from '../auth/decorators/role.decorator';
import { ROLE } from '../../common/constants';
import { FilesInterceptor } from '@nestjs/platform-express';
import { ApiPaginage } from '../../common/decorator/api-paginage.decorator';
import { User } from '../auth/decorators/user.decorator';
import { ProductInfo } from '../order/dto/create-order.dto';

@ApiTags('Product')
@Controller('product')
@ApiBearerAuth()
@UseGuards(JWTAuthGuard, RolesGuard)
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Roles(ROLE.ADMIN)
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FilesInterceptor('images'))
  @Post()
  async createProduct(
    @UploadedFiles() files: Express.Multer.File[],
    @Body() createProductDto: CreateProductDto,
  ) {
    return await this.productService.createProduct(files, createProductDto);
  }

  @Get()
  @ApiPaginage()
  @ApiQuery({ name: 'catalogId', required: false })
  async findAllProduct(
    @Query('page') page: string,
    @Query('limit') limit: string,
    @Query('keyword') keyword: string,
    @Query('catalogId') catalogId: number,
    @Query('sort', SortOptionPipe) sort: FindOptionsOrder,
  ) {
    return this.productService.findAllProduct({
      keyword: keyword || '',
      limit: Number(limit) || 10,
      page: Number(page) || 1,
      sort: sort || { id: 'DESC' },
      catalogId: catalogId || -1,
    });
  }

  @Get(':id')
  async findOneProduct(@Param('id') id: string) {
    return this.productService.findOneProduct({ id: +id });
  }

  @Post('cart')
  async addToCart(@User() user, @Body() data: ProductInfo) {
    return await this.productService.addToCart(user.id, data);
  }

  @Patch('cart/update')
  async updateCart(@User() user, @Body() data: CartDto) {
    return await this.productService.updateCart(user.id, data);
  }

  @Get('cart/array')
  async getCart(@User() user) {
    return await this.productService.getCart(user.id);
  }

  @Roles(ROLE.ADMIN)
  @Patch(':id')
  async updateProduct(
    @Param('id') id: string,
    @Body() updateProductDto: UpdateProductDto,
  ) {
    return this.productService.updateProduct({ id: +id }, updateProductDto);
  }

  @Roles(ROLE.ADMIN)
  @Delete(':id')
  async deleteProduct(@Param('id') id: string) {
    return this.productService.deleteProduct({ id: +id });
  }
}
