import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { ProductRepository } from './product.repository';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/user.module';
import { CloudinaryModule } from '@app/cloudinary';
import { CatalogModule } from '../catalog/catalog.module';
import { CachingModule } from '@app/caching/caching.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    CloudinaryModule,
    CatalogModule,
    CachingModule,
  ],
  controllers: [ProductController],
  providers: [ProductService, ProductRepository],
  exports: [ProductService],
})
export class ProductModule {}
