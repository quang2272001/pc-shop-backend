import { Product } from '@/database/entities/product.entity';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class ProductRepository extends Repository<Product> {
  constructor(datasource: DataSource) {
    super(Product, datasource.createEntityManager());
  }
}
