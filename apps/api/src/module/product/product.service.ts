import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { CartDto, UpdateProductDto } from './dto/update-product.dto';
import { ProductRepository } from './product.repository';
import { FindOptionsWhere, In, Like } from 'typeorm';
import { Product } from '@/database/entities/product.entity';
import { CloudinaryService } from '@app/cloudinary';
import { CatalogService } from '../catalog/catalog.service';
import { ProductInfo } from '../order/dto/create-order.dto';
import { CachingService } from '@app/caching/caching.service';
import { FindAllProductParam } from './dto/find-product.dto';
import { ERROR_MESSAGE } from '../../common/constants';

@Injectable()
export class ProductService {
  constructor(
    private readonly productRepository: ProductRepository,
    private readonly cloudinaryService: CloudinaryService,
    private readonly catalogService: CatalogService,
    private readonly cachingService: CachingService,
  ) {}

  async createProduct(
    files: Express.Multer.File[],
    createProductDto: CreateProductDto,
  ) {
    const images = await this.cloudinaryService.uploadListImage(files);
    const { catalogIds, ...productData } = createProductDto;
    const product = await this.productRepository.save({
      ...productData,
      images,
    });
    await this.catalogService.addProductToCatalog(product.id, catalogIds);
    return {
      success: true,
    };
  }

  async findProductIn(productIds: number[]) {
    return this.productRepository.find({
      where: { id: In(productIds), isDelete: false },
    });
  }

  async findAllProduct(param: FindAllProductParam) {
    const { keyword, limit, page, sort, catalogId } = param;
    const skip = (page - 1) * limit;
    const sortByCatalog = catalogId != -1 ? { catalogId } : {};
    const [data, total] = await this.productRepository.findAndCount({
      where: {
        name: Like(`%${keyword}%`),
        isDelete: false,

        listCatalog: {
          ...sortByCatalog,
        },
      },
      relations: { listCatalog: { catalog: true } },
      take: skip,
      order: sort,
    });
    const totalPage = Math.ceil(total / limit);
    return {
      data,
      paginage: {
        page,
        totalPage,
      },
    };
  }

  async findOneProduct(where: FindOptionsWhere<Product>) {
    const product = await this.productRepository.findOne({
      where: {
        ...where,
        isDelete: false,
      },
      relations: { listCatalog: true },
    });
    const { listCatalog, ...data } = product;
    if (!product)
      throw new BadRequestException(ERROR_MESSAGE.product_not_found);
    return {
      success: true,
      data: {
        ...data,
        catalogIds: listCatalog.map(({ catalogId }) => catalogId),
      },
    };
  }

  async updateProduct(
    where: FindOptionsWhere<Product>,
    updateProductDto: UpdateProductDto,
  ) {
    const { catalogIds, ...updateData } = updateProductDto;
    const product = await this.findOneProduct(where);
    this.catalogService.deleteProductCatalog(
      product.data.catalogIds.filter((v) => !catalogIds.includes(v)),
    );
    this.catalogService.addProductToCatalog(
      product.data.id,
      catalogIds.filter((v) => !product.data.catalogIds.includes(v)),
    );
    await this.productRepository.update(where, updateData);
    return {
      success: true,
    };
  }

  async updateQuantityProduct(products: Product[]) {
    await this.productRepository.save(products);
  }

  async deleteProduct(where: FindOptionsWhere<Product>) {
    await this.findOneProduct(where);
    await this.productRepository.update(where, { isDelete: true });
    return {
      success: true,
    };
  }

  async getCart(userId: number) {
    const cart: Array<ProductInfo> = JSON.parse(
      await this.cachingService.get(String(userId)),
    );
    if (!cart)
      return {
        success: true,
        data: [],
      };
    const products = await this.findProductIn(cart.map(({ id }) => id));
    return {
      success: true,
      data: products.map((v, i) => {
        return {
          product: v,
          quantity: cart[i].quantity,
        };
      }),
    };
  }

  async addToCart(userId: number, data: ProductInfo) {
    await this.findOneProduct({ id: data.id });
    const products: Array<ProductInfo> = JSON.parse(
      await this.cachingService.get(String(userId)),
    );
    if (!products) {
      await this.cachingService.set(String(userId), JSON.stringify([data]));
      return {
        success: true,
      };
    }
    const index = products.findIndex((p) => p.id == data.id);
    if (index != -1) {
      products[index].quantity += data.quantity;
      await this.cachingService.set(String(userId), JSON.stringify(products));
      return {
        success: true,
      };
    }
    await this.cachingService.set(
      String(userId),
      JSON.stringify([...products, data]),
    );
    return {
      success: true,
    };
  }

  async updateCart(userId: number, data: CartDto) {
    await this.cachingService.set(String(userId), JSON.stringify(data.cart));
    return { success: true };
  }
}
