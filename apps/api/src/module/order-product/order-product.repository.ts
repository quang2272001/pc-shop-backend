import { OrderProduct } from '@/database/entities/order-product.entity';
import { Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class OrderProductRepository extends Repository<OrderProduct> {
  constructor(datasource: DataSource) {
    super(OrderProduct, datasource.createEntityManager());
  }
}
