import { Module } from '@nestjs/common';
import { OrderProductService } from './order-product.service';
import { OrderProductController } from './order-product.controller';
import { OrderProductRepository } from './order-product.repository';

@Module({
  controllers: [OrderProductController],
  providers: [OrderProductService, OrderProductRepository],
  exports: [OrderProductService],
})
export class OrderProductModule {}
