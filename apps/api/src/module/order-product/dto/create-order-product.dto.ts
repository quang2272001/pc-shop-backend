import { ProductInfo } from '../../order/dto/create-order.dto';

export class CreateOrderProductDto {
  orderId: number;
  productIds: ProductInfo[];
}
