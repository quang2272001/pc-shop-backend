import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto, UpdateUserDto } from './dto/user.dto';
import { ApiBearerAuth, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { hashPassword } from '../../common/functions';
import { JWTAuthGuard } from '../auth/jwt-auth.guard';
import { Roles } from '../auth/decorators/role.decorator';
import { ROLE } from '../../common/constants';
import { RolesGuard } from '../auth/role/role.guard';
import { SortOptionPipe } from '../../common/validation.pipe';
import { FindOptionsOrder } from '../../common/dto/find.dto';

@ApiTags('User')
@Controller('user')
@UseGuards(JWTAuthGuard, RolesGuard)
@ApiBearerAuth()
export class UserController {
  constructor(private readonly userService: UserService) {}
  @Post()
  async createUser(@Body() data: CreateUserDto) {
    await this.userService.createUser({
      ...data,
      dob: data.dob || new Date(),
      password: await hashPassword(data.password),
    });
    return {
      success: true,
    };
  }

  @Get()
  @Roles(ROLE.ADMIN)
  @ApiQuery({ name: 'page', required: false })
  @ApiQuery({ name: 'limit', required: false })
  @ApiQuery({ name: 'keyword', required: false })
  @ApiQuery({ name: 'sort', required: false })
  async findAllUser(
    @Query('page') page: number,
    @Query('limit') limit: number,
    @Query('keyword') keyword: string,
    @Query('sort', SortOptionPipe) sort: FindOptionsOrder,
  ) {
    return await this.userService.findAllUser({
      page: page || 1,
      limit: limit || 10,
      keyword: keyword || '',
      sort: sort || { id: 'DESC' },
    });
  }

  @Get(':userId')
  async findOneUser(@Param('userId') userId: number) {
    const user = await this.userService.findOneUser({ id: userId });
    return {
      success: true,
      data: user,
    };
  }

  @Patch(':userId')
  async updateUser(
    @Param('userId') userId: number,
    @Body() data: UpdateUserDto,
  ) {
    return await this.userService.updateUser({ id: userId }, data);
  }
}
