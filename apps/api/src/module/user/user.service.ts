import { BadRequestException, Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { CreateUserDto } from './dto/user.dto';
import { User } from '@/database/entities/user.entity';
import { ERROR_MESSAGE } from '../../common/constants';
import { DeepPartial, FindOptionsWhere, Like } from 'typeorm';
import { FindAllParam } from '../../common/dto/find.dto';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  public async createUser(data: CreateUserDto) {
    const userFindByEmail = await this.userRepository.findOneBy({
      email: data.email,
    });
    const userFindByUserName = await this.userRepository.findOneBy({
      username: data.username,
    });
    if (userFindByEmail || userFindByUserName) {
      throw new BadRequestException(ERROR_MESSAGE.username_or_email_existed);
    }

    await this.userRepository.save(
      new User(
        data.email,
        data.password,
        data.fullname,
        data.username,
        data.address,
        data.dob,
        data.phoneNumber,
        data.role,
      ),
    );
  }

  public async findOneUser(where: FindOptionsWhere<User>) {
    const user = await this.userRepository.findOneBy(where);
    if (!user) throw new BadRequestException();
    return user;
  }

  async findAllUser(param: FindAllParam) {
    const { page, limit, keyword, sort } = param;
    const take = (page - 1) * limit;
    const [data, total] = await this.userRepository.findAndCount({
      where: [
        {
          fullname: Like(`%${keyword}%`),
        },
        {
          username: Like(`%${keyword}%`),
        },
        {
          email: Like(`%${keyword}%`),
        },
      ],
      take,
      order: sort,
    });
    const totalPage = Math.ceil(total / limit);
    return {
      success: true,
      data: data.map(({ password, ...rest }) => rest),
      paginage: {
        page,
        totalPage,
      },
    };
  }

  async updateUser(where: FindOptionsWhere<User>, data: DeepPartial<User>) {
    await this.findOneUser(where);
    await this.userRepository.update(where, data);
    return {
      success: true,
    };
  }
}
