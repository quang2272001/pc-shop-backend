import { ApiProperty } from '@nestjs/swagger';
import { ROLE } from 'apps/api/src/common/constants';
import {
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
} from 'class-validator';

export class CreateUserDto {
  @ApiProperty({ required: true })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  password: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  fullname: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  username: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  address: string;

  @ApiProperty({ required: false })
  @IsDateString()
  @IsOptional()
  dob: Date;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumberString()
  phoneNumber: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsEnum(ROLE)
  role: number;
}
export class UpdateUserDto {
  @ApiProperty({ required: true })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  fullname: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  username: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  address: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumberString()
  phoneNumber: string;

  @ApiProperty({ required: true })
  @IsOptional()
  @IsEnum(ROLE)
  role: number;
}
