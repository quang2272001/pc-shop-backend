export default () => ({
  env: process.env.NODE_ENV || 'development',
  port: Number(process.env.PORT) || 8080,
  prefix: process.env.PREFIXSTRING || 'api',
  database: {
    mysql: {
      type: 'mysql',
      username: process.env.DATABASE_USER_NAME || 'root',
      password:
        process.env.DATABASE_PASSWORD || 'afa-fd2B42h6Ed241361A4d41hF-Agb1',
      database: process.env.DATABASE_NAME || 'railway',
      port: Number(process.env.DATABASE_PORT) || 20758,
      host: process.env.DATABASE_HOST || 'monorail.proxy.rlwy.net',
      logging: true,
    },
  },
  auth: {
    jwtsecret: process.env.JWT_SECRET || 'bsdfbsubfaksuufsbsdbsdjbdsfbadsk',
    access_token_ttl: process.env.ACCESS_TOKEN_TTL || '1d',
  },
});
