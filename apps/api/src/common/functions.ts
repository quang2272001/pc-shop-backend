import * as bcrypt from 'bcrypt';

export async function hashPassword(password: string) {
  const saltOrRounds = 10;
  const hash = await bcrypt.hash(password, saltOrRounds);
  return hash;
}

export function comparePassword(password, hashPassword) {
  return bcrypt.compareSync(password, hashPassword);
}
