export enum ROLE {
  ADMIN = 0,
  USER = 1,
}

export enum ERROR_MESSAGE {
  username_or_email_existed = 'username_or_email_existed',
  username_or_password_incorect = 'username_or_password_incorect',
  user_forbiden = 'user_forbiden',
  product_not_found = 'product_not_found',
}

export enum ORDER_STATUS {
  created = 'created',
  shipped = 'shipped',
  received = 'received',
  cancled = 'cancled',
  all = '',
}

export enum RECEIVE_TYPE {
  RECEIVE_AT_HOME = 0,
  RECEIVE_AT_STORE = 1,
}
