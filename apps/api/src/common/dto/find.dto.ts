export interface FindAllParam {
  page: number;
  limit: number;
  keyword: string;
  sort?: FindOptionsOrder;
}

export interface FindOptionsOrder {
  [key: string]: SortValue | FindOptionsOrder;
}

export type SortValue = 'ASC' | 'DESC' | 'asc' | 'desc';
