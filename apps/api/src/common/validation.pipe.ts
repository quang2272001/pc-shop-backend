import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { FindOptionsOrder } from './dto/find.dto';

@Injectable()
export class SortOptionPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata): FindOptionsOrder {
    if (!value) return null;
    const result = {};
    const options = value.split(',');
    for (const option of options) {
      const [field, sort] = option.trim().split(':');
      result[field] = sort;
    }
    return result;
  }
}
